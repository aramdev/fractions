export function calc (x1, x2, y1, y2) {
  const numerator = (x1 * y2) + (y1 * x2)
  const denominator = x2 * y2

  return {
    numerator,
    denominator
  }
}


